const url = 'https://ajax.test-danit.com/api/swapi/films';

const div = document.querySelector('#films');
const ul = document.createElement('ul');


div.prepend(ul);

fetch(url, {
    method: 'get',
    headers: {
      'Content-type': 'application/json'
    },
    
  }).then((data) => {
    return data.json();
  })
  .then(data => {
        data.forEach(film => {
          ul.insertAdjacentHTML('beforeend',`<li>${film.episodeId},${film.name}</li>, <br>`)
          ul.insertAdjacentHTML('beforeend',`<li>${film.openingCrawl}</li>, <br>`)
       
        Promise.all(film.characters.map(characterUrl =>
            fetch(characterUrl)
            .then((data) => {
                return data.json();
            })
            ))
            .then(charactersData => {
            charactersData.forEach(character => {
              ul.insertAdjacentHTML('beforeend',`<li>${character.name}</li>`)
            });
            ul.insertAdjacentHTML('beforeend',`<br>`)
          })
          .catch(error => console.error('Error fetching characters:', error));
        });
      })
      .catch(error => console.error('Error fetching films:', error));

